.. _sphinx-extensions:

==============================
Sphinx extensions in Salt docs
==============================

Sphinx extensions add additional functionality to both the Sphinx docs and to
the Furo documentation theme. It's a best practice to check
`Pypi <https://pypi.org/>`_ or their extension's repository on a regular basis
to ensure that we're using the latest versions. Update the latest versions in
the ``requirements.txt`` file for each documentation repository that uses the
standard tech stack (Sphinx, Furo, etc.).


sphinx-copybutton
=================
Creates a copy button by all code blocks that allows users to copy the code to
their clipboard.

* Repo and docs: `sphinx-copybutton repo <https://github.com/executablebooks/sphinx-copybutton>`_


sphinx-design
=============
One of our most important extensions, Sphinx design adds significant
functionality to our documentation sets.

Check out their docs for a demo of all that Sphinx design does. The Salt docs
use a lot of their features, but not all. If you need a new function, it's
usually possible to do it with Sphinx design or the Furo theme.

* Docs: `Sphinx Design docs <https://sphinx-design.readthedocs.io/en/furo-theme/>`_
* Repo: `sphinx-design repo <https://github.com/executablebooks/sphinx-design>`_


sphinx-inline-tabs
==================
Sphinx inline tabs is the default tab system used by the Furo theme for adding
tabbed content and it has the same maintainer.

.. Note::
    I've gotten some negative feedback from the community that these tabs
    aren't as easy to use as the ``sphinx-tabs`` solution.

* Repo: `Sphinx inline tabs <https://github.com/pradyunsg/sphinx-inline-tabs>`_
* Docs and demo: `Furo tabs <https://pradyunsg.me/furo/reference/tabs/>`_


sphinx-prompt
=============
This extension is a dependency for Sphinx Substitution Extensions. It is a
directive to add an prompt that can't be selected.

* Repo and docs: `Sphinx prompt <https://github.com/sbrunner/sphinx-prompt>`_


Sphinx-Substitution-Extensions
==============================
Allows you to use
`Sphinx substitutions <https://sublime-and-sphinx-guide.readthedocs.io/en/latest/reuse.html>`_
inside code blocks.

FYI, the source for Sphinx substitutions in our Salt docs can be found in the
``docs/sitevars.rst`` file in every documentation repo. You tell Sphinx to use
that file as the source for your substitutions in the ``conf.py`` file.

* Repo and docs: `Sphinx-Substitution-Extensions <https://github.com/adamtheturtle/sphinx-substitution-extensions>`_


sphinx-tabs
===========
Like the ``sphinx-inline-tabs`` extension, this extension is used for creating
tabbed content in the documentation.

.. Note::
    Our community prefers this tab style over the one used in Furo.

* Repo and docs: `sphinx-tabs <https://github.com/executablebooks/sphinx-tabs>`_
