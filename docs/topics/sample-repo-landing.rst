:hide-toc:
:sd_hide_title:


.. div:: landing-title

    .. grid::
        :reverse:
        :gutter: 2 3 3 3
        :margin: 4 4 1 2

        .. grid-item::
            :columns: 12 3 3 3
            :class: sd-m-auto sd-animate-grow50-rot20

            .. image:: ../_static/img/repo-landing-small.png
              :width: 125
              :alt: Salt Project repo logo

        .. grid-item::
            :columns: 12 9 9 9
            :class: sd-text-white sd-fs-2 sd-text-center

            The Salt Project Repository

            .. button-link:: https://repo.saltproject.io/py3/
               :color: light
               :align: center
               :outline:

                Browse the directory of package files



=================
Salt Project repo
=================

Install Salt
============

.. grid:: 3

    .. grid-item::

        .. button-link:: https://docs.saltproject.io/salt/install-guide/en/latest/
           :color: primary
           :expand:

           Salt install guide :octicon:`arrow-up-right`

    .. grid-item::

        .. button-link:: https://docs.saltproject.io/salt/install-guide/en/latest/topics/bootstrap.html
           :color: primary
           :expand:

           Bootstrap install :octicon:`arrow-up-right`

    .. grid-item::

        .. button-link:: https://docs.saltproject.io/salt/install-guide/en/latest/topics/salt-supported-operating-systems.html
           :color: primary
           :expand:

           Supported systems :octicon:`arrow-up-right`

Quick install by operating system
=================================

.. dropdown:: What is onedir vs. classic?
    :color: primary

    Beginning with the 3005 (Phosphorus) release of Salt, the Salt Project is
    changing its packaging system to onedir. Any new operating systems added in
    3005 will only have onedir packages. The classic packaging system will only
    be supported until the 3007 release. See
    `What is onedir? <https://docs.saltproject.io/salt/install-guide/en/latest/topics/upgrade-to-onedir.html>`_
    for more information and upgrade instructions.

Operating systems are listed in alphabetical order:

.. grid:: 3
    :gutter: 3

    .. grid-item-card:: AIX
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`AIX install guide (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/aix.html>`

    .. grid-item-card:: Amazon Linux 2
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Onedir install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/amazon.html#install-onedir-packages-of-salt-on-amazon-linux-2>`
       :bdg-link-primary-line:`Classic install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/amazon.html#install-classic-packages-of-salt-on-amazon-linux-2>`

    .. grid-item-card:: Arista
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Arista install guide (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/arista.html>`

    .. grid-item-card:: Arch Linux
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Arch Linux install (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/arch.html>`

    .. grid-item-card:: CentOS
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Onedir Centos 9 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/centos.html#install-onedir-packages-of-salt-on-centos-9>`
       :bdg-link-primary:`Onedir Centos 8 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/centos.html#install-onedir-packages-of-salt-on-centos-8>`
       :bdg-link-primary:`Onedir Centos 7 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/centos.html#install-onedir-packages-of-salt-on-centos-7>`
       :bdg-link-primary-line:`Classic Centos 7 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/centos.html#install-classic-packages-of-salt-on-centos-7>`

    .. grid-item-card:: Debian
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Onedir Debian 11 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/debian.html#install-onedir-packages-of-salt-on-debian-11-bullseye>`
       :bdg-link-primary-line:`Classic Debian 11 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/debian.html#install-classic-packages-of-salt-on-debian-11-bullseye>`
       :bdg-link-primary-line:`Classic Debian 11 ARM64 install <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/debian.html#install-classic-packages-of-salt-on-debian-11-bullseye-arm64>`
       :bdg-link-primary-line:`Classic Debian 10 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/debian.html#install-classic-packages-of-salt-on-debian-10-buster>`

    .. grid-item-card:: Fedora
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Fedora install guide (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/fedora.html>`

    .. grid-item-card:: FreeBSD
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`FreeBSD install guide (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/freebsd.html>`

    .. grid-item-card:: Juniper (JunOS)
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Juniper install guide (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/juniper.html>`

    .. grid-item-card:: macOS
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`macOS install guide (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/macos.html>`

    .. grid-item-card:: Oracle Linux
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Oracle Linux install (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/oracle.html>`

    .. grid-item-card:: Raspbian
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Raspbian install guide (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/raspbian.html>`

    .. grid-item-card:: RedHat (RHEL)
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Onedir RHEL 9 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/rhel.html#install-onedir-packages-of-salt-on-redhat-rhel-9>`
       :bdg-link-primary:`Onedir RHEL 8 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/rhel.html#install-onedir-packages-of-salt-on-redhat-rhel-8>`
       :bdg-link-primary:`Onedir RHEL 7 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/rhel.html#install-onedir-packages-of-salt-on-redhat-rhel-7>`
       :bdg-link-primary-line:`Classic RHEL 8 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/rhel.html#install-classic-packages-of-salt-on-redhat-rhel-8>`
       :bdg-link-primary-line:`Classic RHEL 7 install guide <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/rhel.html#install-classic-packages-of-salt-on-redhat-rhel-7>`

    .. grid-item-card:: Solaris
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Solaris install guide (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/solaris.html>`

    .. grid-item-card:: SUSE (SLES)
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`SUSE install guide (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/sles.html>`

    .. grid-item-card:: Ubuntu
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Onedir Ubuntu 22.04 install <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/ubuntu.html#install-onedir-packages-of-salt-on-ubuntu-22-jammy>`
       :bdg-link-primary:`Onedir Ubuntu 20.04 install <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/ubuntu.html#install-onedir-packages-of-salt-on-ubuntu-20-focal>`
       :bdg-link-primary:`Onedir Ubuntu 18.04 install <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/ubuntu.html#install-onedir-packages-of-salt-on-ubuntu-18-bionic>`
       :bdg-link-primary-line:`Classic Ubuntu 20.04 install <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/ubuntu.html#install-classic-packages-of-salt-on-ubuntu-20-focal>`
       :bdg-link-primary-line:`Classic Ubuntu 20.04 ARM64 <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/ubuntu.html#install-classic-packages-of-salt-on-ubuntu-20-focal-arm64>`
       :bdg-link-primary-line:`Classic Ubuntu 18.04 install <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/ubuntu.html#install-classic-packages-of-salt-on-ubuntu-18-bionic>`

    .. grid-item-card:: Windows
       :class-card: sd-border-1
       :shadow: md

       :bdg-link-primary:`Windows install (all versions) <https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/windows.html>`
