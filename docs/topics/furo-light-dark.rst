.. _furo-light-dark:

===========================
Furo's light and dark modes
===========================

To customize Furo's light and dark modes, you have to understand a little bit
about how browsers detect the user's preference for dark and light modes. You
also have to understand how CSS variables work.

The following videos help explain how these concepts work:

* `Light & Dark Mode with CSS Variables #1 - Intro & Setup <https://www.youtube.com/watch?v=6YrOGKmGTCY&ab_channel=TheNetNinja>`_
* `Light & Dark Mode with CSS Variables #2 - Default Light/Dark Modes <https://www.youtube.com/watch?v=pZhAiCSqHPs&ab_channel=TheNetNinja>`_


Testing light vs. dark
======================
Firefox seems to be the easiest for switching between light and dark browser
modes. See
`How to Enable Dark Mode in Firefox <https://www.howtogeek.com/359033/how-to-enable-dark-mode-in-firefox/>`_
for more information.


Setting variables
=================
To see dark and light mode variables in action, check out the All Salt Docs
CSS style sheet in this repository and see the Landing Page styles.

Some options can also be set in the `conf.py` file. For example, this
documentation set has set the dark mode hyperlinks using this code:

.. code-block:: python

    html_theme_options = {
        "dark_css_variables": {
            "color-brand-primary": "#66CCF4",
            "color-brand-content": "#66CCF4",
        },
    }

See `Changing colors <https://pradyunsg.me/furo/customisation/colors/>`_ in the
Furo docs for more information.
